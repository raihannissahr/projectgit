package com.raihannissapaylater.service;

import com.raihannissapaylater.dto.TransaksiRequest;
import com.raihannissapaylater.dto.TransaksiResponse;
import com.raihannissapaylater.entity.Barang;
import com.raihannissapaylater.entity.Menu;
import com.raihannissapaylater.entity.Pelanggan;
import com.raihannissapaylater.entity.Transaksi;
import com.raihannissapaylater.repository.BarangRepository;
import com.raihannissapaylater.repository.MenuRepository;
import com.raihannissapaylater.repository.PelangganRepository;
import com.raihannissapaylater.repository.TransaksiRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TransaksiService {
    private final MenuRepository menuRepository;
    private final TransaksiRepository transaksiRepository;
    private final BarangRepository barangRepository;
    private final PelangganRepository pelangganRepository;

    public List<Menu> getAllMenu(){
        return menuRepository.findAll();
    }


    @SneakyThrows
    public TransaksiResponse saveTransaksi(TransaksiRequest request){

        Optional<Pelanggan> pelangganOptional= pelangganRepository.findById(request.getPelangganId());
        if(!pelangganOptional.isPresent()){
            throw new IllegalArgumentException();
        }
        Pelanggan pelanggan = pelangganOptional.get();


        Optional<Barang> barangOptional = barangRepository.findById(request.getBarangId());

        if(!barangOptional.isPresent()){
            throw new IllegalArgumentException();
        }
        Barang barang = barangOptional.get();

        Transaksi transaksi = new Transaksi();
        transaksi.setNomorPembayaran(request.getNomorPembayaran());
        transaksi.setPelanggan(pelanggan);
        transaksi.setJatuhTempo(request.getJatuhTempo());
        transaksi.setTotalTagihan(barang.getHargaBarang().add(new BigDecimal(10000)));
        transaksi.setBarang(barang);
        transaksiRepository.save(transaksi);

        TransaksiResponse response = new TransaksiResponse();
        response.setId(transaksi.getId());
        response.setNomorPembayaran(transaksi.getNomorPembayaran());
        response.setTotalTagihan(transaksi.getTotalTagihan());
        response.setJatuhTempo(transaksi.getJatuhTempo());
        return response;
    }

    @SneakyThrows
    public TransaksiResponse updateTransaksi(Integer id, TransaksiRequest request){
        Optional<Transaksi> barang = transaksiRepository.findById(id);
        Transaksi trk = new Transaksi();
        if(!barang.isPresent()){
            throw new IllegalArgumentException();
        }else{
            trk.setId(id);
            trk.setNomorPembayaran(request.getNomorPembayaran());
            trk.setJatuhTempo(request.getJatuhTempo());

        }
        TransaksiResponse response = new TransaksiResponse();
        response.setId(trk.getId());
        response.setNomorPembayaran(trk.getNomorPembayaran());
        response.setTotalTagihan(trk.getTotalTagihan());
        response.setJatuhTempo(trk.getJatuhTempo());
        return response;
    }

@SneakyThrows
    public List<Transaksi> getAll() {
        List<Transaksi> listTransaksi = null;

        listTransaksi = transaksiRepository.findAll();

        return listTransaksi;
    }

    public Optional<Transaksi> getAllById(Integer id) {
        Optional<Transaksi> listTransaksi;

        listTransaksi = transaksiRepository.findById(id);

        return listTransaksi;
    }

    //
    public String deleteTransaksi(Integer id){
        String response = "";
        try {
            transaksiRepository.deleteById(id);
            response = "Delete success...";
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return response;
    }

    public List<Transaksi> getTransaksiByIDPelanggan(Integer idPelanggan){

        return  transaksiRepository.getAllTransaksiByPelangganId(idPelanggan);
    }
}
