package com.raihannissapaylater.service;

import com.raihannissapaylater.dto.login.LoginRequest;
import com.raihannissapaylater.dto.login.LoginResponse;
import com.raihannissapaylater.entity.Login;
import com.raihannissapaylater.repository.LoginRepository;
import com.raihannissapaylater.util.JwtToken;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class LoginService {

    @Autowired
    private LoginRepository loginRepository;

    @SneakyThrows
    public LoginResponse loginPelanggan(LoginRequest request){
        Optional<Login> login = loginRepository.findByUserNameAndPassword(request.getUsername(), request.getPassword());
        boolean usernames = login.isPresent();
        Login l = new Login();

        if(usernames){
            l.setId(login.get().getId());
            l.setUserName(login.get().getUserName());
            l.setPassword(login.get().getPassword());
            l.setTanggalLogin(LocalDateTime.now());
            l.setToken(JwtToken.getToken(request));
            loginRepository.save(l);
        }
        LoginResponse response = new LoginResponse();
        response.setToken(l.getToken());
        return response;
    }

}
