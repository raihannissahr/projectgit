package com.raihannissapaylater.service;

import com.raihannissapaylater.dto.login.LoginRequest;
import com.raihannissapaylater.dto.pelanggan.PelangganRequest;
import com.raihannissapaylater.dto.pelanggan.PelangganResponse;
import com.raihannissapaylater.entity.Login;
import com.raihannissapaylater.entity.Pelanggan;
import com.raihannissapaylater.repository.LoginRepository;
import com.raihannissapaylater.repository.PelangganRepository;
import com.raihannissapaylater.util.JwtToken;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PelangganService {


    private final PelangganRepository pelangganRepository;
    private final LoginRepository loginRepository;
    private final TokenService tokenService;

    @SneakyThrows
    public PelangganResponse savePelanggan(String token, PelangganRequest request) {
        if (!tokenService.getToken(token)){
            return null;
        }
        Pelanggan pelanggan = new Pelanggan();
        pelanggan.setNama(request.getNama());
        pelanggan.setAlamat(request.getAlamat());
        pelanggan.setEmail(request.getEmail());
        pelanggan.setSaldo(request.getSaldo());
        pelangganRepository.save(pelanggan);

        //registrasi
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(request.getUserName());
        loginRequest.setPassword(request.getPassword());
        Login login = new Login();
        Optional<Pelanggan> pelangganOptional = pelangganRepository.findById(pelanggan.getId());
        if (!pelangganOptional.isPresent()) {
            throw new IllegalArgumentException();
        }
        login.setPelanggan(pelangganOptional.get());
        login.setUserName(request.getUserName());
        login.setPassword(request.getPassword());
        login.setToken(JwtToken.getToken(loginRequest));
        loginRepository.save(login);

        PelangganResponse response = new PelangganResponse();
        response.setId(pelanggan.getId());
        response.setNama(pelanggan.getNama());
        response.setAlamat(pelanggan.getAlamat());
        response.setEmail(pelanggan.getEmail());
        response.setSaldo(pelanggan.getSaldo());


        return response;
    }

    @SneakyThrows
    public PelangganResponse updatePelanggan(Integer id, PelangganRequest request) {
        Optional<Pelanggan> pelanggan = pelangganRepository.findById(id);
        Pelanggan pgl = new Pelanggan();
        if (!pelanggan.isPresent()) {
            throw new IllegalArgumentException();
        } else {
            pgl.setId(id);
            pgl.setNama(request.getNama());
            pgl.setAlamat(request.getAlamat());
            pgl.setEmail(request.getEmail());
            pgl.setSaldo(request.getSaldo());
        }
        PelangganResponse response = new PelangganResponse();
        response.setId(id);
        response.setNama(request.getNama());
        response.setAlamat(request.getAlamat());
        response.setEmail(request.getEmail());
        response.setSaldo(request.getSaldo());
        return response;
    }

    public String deletePelanggan(Integer id) throws IllegalArgumentException {
        String response = "";
        try {
            pelangganRepository.deleteById(id);
            response = "Delete success...";
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException();
        }
        return response;
    }

    @SneakyThrows
    public List<Pelanggan> getAll() {
        List<Pelanggan> listPelanggan = null;

        listPelanggan = pelangganRepository.findAll();

        return listPelanggan;
    }

    @SneakyThrows
    public Pelanggan getById(Integer id) {
        Optional<Pelanggan> listPelanggan = pelangganRepository.findById(id);
        if (!listPelanggan.isPresent()) {
            throw new IllegalArgumentException();
        }
        return listPelanggan.get();
    }
}
