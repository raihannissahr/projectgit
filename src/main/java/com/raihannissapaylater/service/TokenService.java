package com.raihannissapaylater.service;

import com.raihannissapaylater.entity.Login;
import com.raihannissapaylater.repository.LoginRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class TokenService {

    private final LoginRepository loginRepository;

    public boolean getToken(String token) {
        Optional<Login> getToken = loginRepository.findByToken(token);

        return getToken.isPresent();
    }
}
