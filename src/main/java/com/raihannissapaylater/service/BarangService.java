package com.raihannissapaylater.service;

import com.raihannissapaylater.dto.barang.BarangRequest;
import com.raihannissapaylater.dto.barang.BarangResponse;
import com.raihannissapaylater.entity.Barang;
import com.raihannissapaylater.repository.BarangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BarangService {

    @Autowired
    private BarangRepository barangRepository;

    public BarangResponse saveBarang(BarangRequest request){
        Barang barang = new Barang();
        barang.setNamaBarang(request.getNamaBarang());
        barang.setHargaBarang(request.getHargaBarang());

        barangRepository.save(barang);

        BarangResponse response = new BarangResponse();
        response.setId(barang.getId());
        response.setNamaBarang(barang.getNamaBarang());
        response.setHargaBarang(barang.getHargaBarang());
        return response;
    }

    public BarangResponse updateBarang(Integer id, BarangRequest request){
        Optional<Barang> barang = barangRepository.findById(id);
        Barang brg = new Barang();
        if(!barang.isPresent()){
            throw new IllegalArgumentException();
        }else{
            brg.setId(id);
            brg.setNamaBarang(request.getNamaBarang());
            brg.setHargaBarang(request.getHargaBarang());
        }
        BarangResponse response = new BarangResponse();
        response.setId(barang.get().getId());
        response.setNamaBarang(barang.get().getNamaBarang());
        response.setHargaBarang(barang.get().getHargaBarang());
        return response;
    }

    public String deleteBarang(Integer id){
        barangRepository.deleteById(id);
        return "Hapus Berhasil";
    }


    public List<Barang> getAll() {
        List<Barang> listBarang = null;

        listBarang = barangRepository.findAll();

        return listBarang;
    }


    public Optional<Barang> getAllById(Integer id){

        return (barangRepository.findById(id));
    }
}
