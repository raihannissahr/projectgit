package com.raihannissapaylater.controller;

import com.raihannissapaylater.dto.login.LoginRequest;
import com.raihannissapaylater.dto.login.LoginResponse;
import com.raihannissapaylater.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/paylater/login")
public class LoginController {
    @Autowired
    LoginService loginService;

    @PostMapping
    public ResponseEntity<LoginResponse> login (@RequestBody LoginRequest request){
        LoginResponse response = loginService.loginPelanggan(request);
        return ResponseEntity.ok(response);
    }
}
