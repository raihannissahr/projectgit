package com.raihannissapaylater.controller;

import com.raihannissapaylater.dto.barang.BarangRequest;
import com.raihannissapaylater.dto.barang.BarangResponse;
import com.raihannissapaylater.entity.Barang;
import com.raihannissapaylater.service.BarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/paylater/barang")
public class BarangController {

    @Autowired
    private BarangService barangService;

    @GetMapping
    public ResponseEntity<List<Barang>> getAllBarang() throws RuntimeException {
        List<Barang> response = barangService.getAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Barang>> getAllById(@PathVariable("id") Integer id) throws RuntimeException {
        Optional<Barang> response = barangService.getAllById(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<BarangResponse> addBarang(@RequestBody BarangRequest request) {
        BarangResponse response = barangService.saveBarang(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BarangResponse> updateBarang(@PathVariable("id") Integer id,
                                                       @RequestBody BarangRequest request)
            throws Exception{
        BarangResponse response = barangService.updateBarang(id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteBarang(@PathVariable("id") Integer id) throws Exception{
        String response = barangService.deleteBarang(id);
        return ResponseEntity.ok(response);
    }
}
