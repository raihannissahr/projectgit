package com.raihannissapaylater.controller;

import com.raihannissapaylater.dto.TransaksiRequest;
import com.raihannissapaylater.dto.TransaksiResponse;
import com.raihannissapaylater.entity.Transaksi;
import com.raihannissapaylater.service.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/paylater/order")
public class TransaksiController {

    @Autowired
    private TransaksiService transaksiService;


    @PostMapping("/add-transaksi")
    public ResponseEntity<TransaksiResponse> addTransaksi(@RequestBody TransaksiRequest request){
        TransaksiResponse response = transaksiService.saveTransaksi(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/update-transaksi/{id}")
    public ResponseEntity<TransaksiResponse> updateTransaksi(@PathVariable("id") Integer id, @RequestBody TransaksiRequest request) throws Exception {
        TransaksiResponse response = transaksiService.updateTransaksi(id, request);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/list-transaksi")
    public ResponseEntity<List<Transaksi>> getAll(){
        List<Transaksi> response = transaksiService.getAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<Transaksi>> getTransaksiByPelangganId(@PathVariable("id") Integer id){
        List<Transaksi> response = transaksiService.getTransaksiByIDPelanggan(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
