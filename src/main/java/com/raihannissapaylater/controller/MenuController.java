package com.raihannissapaylater.controller;


import com.raihannissapaylater.entity.Menu;
import com.raihannissapaylater.service.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/paylater/menu")
public class MenuController {

    @Autowired
    private TransaksiService transaksiService;

    @GetMapping("/get-all-menu")
    public ResponseEntity<List<Menu>> getAll(){
        List<Menu> response = transaksiService.getAllMenu();
        return  ResponseEntity.ok(response);
    }
}
