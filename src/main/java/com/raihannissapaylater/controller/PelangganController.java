package com.raihannissapaylater.controller;

import com.raihannissapaylater.dto.RegisterResponse;
import com.raihannissapaylater.dto.pelanggan.PelangganRequest;
import com.raihannissapaylater.dto.pelanggan.PelangganResponse;
import com.raihannissapaylater.entity.Pelanggan;
import com.raihannissapaylater.service.PelangganService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/paylater/pelanggan")
public class PelangganController {

    @Autowired
    private PelangganService pelangganService;


    @GetMapping
    public ResponseEntity<List<Pelanggan>> getAllPelanggan() throws RuntimeException {
        List<Pelanggan> response = pelangganService.getAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Pelanggan> getById(@PathVariable("id") Integer id) {
        Pelanggan response = pelangganService.getById(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/registrasi")
    public ResponseEntity<PelangganResponse> addPelanggan(@Valid @RequestHeader("token") String token , @RequestBody PelangganRequest request) {
        PelangganResponse response = pelangganService.savePelanggan(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PelangganResponse> updatePelanggan(@PathVariable("id") Integer id,
                                                             @RequestBody PelangganRequest request) {
        PelangganResponse response = pelangganService.updatePelanggan(id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePelanggan(@PathVariable("id") Integer id) throws Exception {
        String response = pelangganService.deletePelanggan(id);
        return ResponseEntity.ok(response);
    }
}
