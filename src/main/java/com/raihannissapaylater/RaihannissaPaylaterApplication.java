package com.raihannissapaylater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RaihannissaPaylaterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RaihannissaPaylaterApplication.class, args);
	}

}
