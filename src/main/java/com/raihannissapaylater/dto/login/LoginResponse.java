package com.raihannissapaylater.dto.login;

import lombok.Data;

@Data
public class LoginResponse {
    private String token;
}
