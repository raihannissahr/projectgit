package com.raihannissapaylater.dto.login;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LoginRequest {
    private String username;
    private String password;
//    private LocalDateTime tanggalLogin;
}
