package com.raihannissapaylater.dto;

import lombok.Data;

import java.util.Date;

@Data
public class RegisterResponse {
    private String token;
    private Date tanggalRegister;
}
