package com.raihannissapaylater.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransaksiRequest {
    private Integer nomorPembayaran;
    private String jatuhTempo;
    private Integer pelangganId;
    private Integer barangId;

}
