package com.raihannissapaylater.dto.pelanggan;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PelangganResponse {
    private Integer id;
    private String nama;
    private String alamat;
    private String email;
    private BigDecimal saldo;
    private String token;

}
