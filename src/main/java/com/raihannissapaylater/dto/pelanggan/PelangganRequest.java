package com.raihannissapaylater.dto.pelanggan;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
public class PelangganRequest {
    @NotBlank(message="Please Enter name")
    private String nama;
    @NotBlank(message="Please Enter alamat")
    private String alamat;
    @NotBlank(message="Please Enter password")
    private String password;
    @NotBlank(message="Please Enter email")
    private String email;
    @DecimalMin(value= "1000000", message="Saldo minimal 1jt")
    private BigDecimal saldo;
    @NotBlank(message="Please Enter username")
    private String userName;

}
