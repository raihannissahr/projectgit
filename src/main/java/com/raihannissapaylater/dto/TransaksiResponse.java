package com.raihannissapaylater.dto;


import com.raihannissapaylater.entity.Barang;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransaksiResponse {

    private Integer id;
    private Integer nomorPembayaran;
    private String jatuhTempo;
    private BigDecimal totalTagihan;
}
