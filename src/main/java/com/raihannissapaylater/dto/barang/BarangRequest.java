package com.raihannissapaylater.dto.barang;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BarangRequest {
    private String namaBarang;
    private BigDecimal hargaBarang;

}
