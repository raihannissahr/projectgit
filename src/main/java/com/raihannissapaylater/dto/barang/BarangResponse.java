package com.raihannissapaylater.dto.barang;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BarangResponse {
    private Integer id;
    private String namaBarang;
    private BigDecimal hargaBarang;
}
