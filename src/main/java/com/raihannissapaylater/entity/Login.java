package com.raihannissapaylater.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity

public class Login {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;
    private String userName;
    private String password;
    private LocalDateTime tanggalLogin;
    @Column(length = 1000)
    private String token;

    @ManyToOne
    private Pelanggan pelanggan;
}
