package com.raihannissapaylater.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
public class Transaksi {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer nomorPembayaran;
    private String jatuhTempo;
    private BigDecimal totalTagihan;

    @ManyToOne
    private Pelanggan pelanggan;

    @ManyToOne
    private Barang barang;
}
