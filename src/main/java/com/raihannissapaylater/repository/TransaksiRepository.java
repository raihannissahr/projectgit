package com.raihannissapaylater.repository;


import com.raihannissapaylater.entity.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransaksiRepository extends JpaRepository<Transaksi,Integer> {
    @Query("Select t From Transaksi t Where t.pelanggan.id =:idPelanggan")
    List<Transaksi> getAllTransaksiByPelangganId(@Param("idPelanggan") Integer id);
}
