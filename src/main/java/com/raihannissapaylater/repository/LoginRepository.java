package com.raihannissapaylater.repository;

import com.raihannissapaylater.entity.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoginRepository extends JpaRepository<Login,Integer> {

    Optional<Login> findByUserNameAndPassword(String username, String password);
    Optional<Login> findById(Integer id);
    Optional<Login> findByToken(String token);
}
