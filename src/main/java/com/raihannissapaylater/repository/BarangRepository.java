package com.raihannissapaylater.repository;

import com.raihannissapaylater.entity.Barang;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BarangRepository extends JpaRepository<Barang,Integer> {

}
